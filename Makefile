ARDUINOCORESRC=$(wildcard ${ARDUINOCORE}/*.c)
ARDUINOCORESRCCPP=$(wildcard ${ARDUINOCORE}/*.cpp)
ARDUINOCORESRCASM=$(wildcard ${ARDUINOCORE}/*.S)

ARDUINOCOREOBJ=$(ARDUINOCORESRC:${ARDUINOCORE}/%.c=%.o)
ARDUINOCOREOBJCPP=$(ARDUINOCORESRCCPP:${ARDUINOCORE}/%.cpp=%.o)
ARDUINOCOREOBJASM=$(ARDUINOCORESRCASM:${ARDUINOCORE}/%.s=ASM_%.o)

SRC=$(wildcard src/*.cpp)
OBJ=$(SRC:%.cpp=%.o)

ARCHFLAGS=-DF_CPU=16000000UL -mmcu=atmega328p -DARDUINO=101  # hack
CFLAGS=-I${ARDUINOPINS} -I${ARDUINOCORE} -Isrc/ -Os -std=c++11

%.o: %.cpp
	${AVRGCC}/avr-gcc $(CFLAGS) $(ARCHFLAGS) -c -o $@ $<

%.o: ${ARDUINOCORE}/%.c
	${AVRGCC}/avr-gcc $(CFLAGS) $(ARCHFLAGS) -c -o $@ $<

%.o: ${ARDUINOCORE}/%.cpp
	${AVRGCC}/avr-gcc $(CFLAGS) $(ARCHFLAGS) -c -o $@ $<

ASM_%.o: ${ARDUINOCORE}/%.s
	${AVRGCC}/avr-gcc $(CFLAGS) $(ARCHFLAGS) -c -o $@ $<


app: $(ARDUINOCOREOBJ) $(ARDUINOCOREOBJCPP) $(ARDUINOCOREOBJASM) $(OBJ)
	${AVRGCC}/avr-gcc $(ARCHFLAGS) -o $@ $^
	${AVRGCC}/avr-objcopy -O ihex -R .eeprom $@ $@.hex

flash: app
	${AVRGCC}/avrdude -C${ARDUINOTOOLS}/etc/avrdude.conf -v -patmega328p -carduino -P/dev/ttyUSB0 -b57600 -D -Uflash:w:$^.hex:i
