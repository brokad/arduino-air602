{ nixpkgs ? import <nixpkgs> {} }:

with nixpkgs;

stdenv.mkDerivation {

  name = "arduino-air602";
  version = "0.1";

  ARDUINOCORE = "${arduino-core}/share/arduino/hardware/arduino/avr/cores/arduino";
  ARDUINOPINS = "${arduino-core}/share/arduino/hardware/arduino/avr/variants/standard";
  AVRGCC = "${arduino-core}/share/arduino/hardware/tools/avr/bin";
  ARDUINOTOOLS = "${arduino-core}/share/arduino/hardware/tools/avr";

  nativeBuildInputs = with nixpkgs; [
    arduino-core
  ];

  src = ./src;

}
