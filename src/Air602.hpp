#ifndef __AIR602_HPP__
#define __AIR602_HPP__

#include <ATcommands.hpp>
#include <queue.hpp>


#define AIR602_DELAY_BEFORE_RETRY 10000
#define AIR602_DEFAULT_DELAY 1000


class Air602 {


public:

  class Command;

  enum MQTTPayloadType
    {
     MESSAGE,     // a MQTT message
     MQTTRESULT,  // MQTT: <result>
     MSUBRESULT,  // MSUB: <result>
     MPUBRESULT   // MPUB: <result>
    };

  struct MQTTPayload {
    MQTTPayloadType type;
    String topic;  // populated only when type == MESSAGE, otherwise irrelevant
    String data;
  };

private:

  struct Transaction {
    Command* command;
    unsigned long sent;
  };

  Queue<Transaction> queue;
  MQTTPayload* bufferMQTT = 0;
  void (*mqttCallback)(Air602&, const MQTTPayload&) = 0;

  bool enqueue( Command& command, unsigned long delay );
  void handleMQTT( AT::Payload& payload );


public:


  class Operation {

  public:

    enum State
      {
       PENDING,
       OK,
       ERR
      };

    enum Logic
      {
       OR,
       AND
      };

  protected:

    friend Air602;

    Operation* parent = 0;

    virtual void step( Air602& mgr ) = 0;
    void complete( Air602& mgr, State state, int errcode );

    State state = PENDING;
    int errcode = 0;

    unsigned long delay_ms = AIR602_DEFAULT_DELAY;

  public:

    AT::Payload* payload = 0;  // td: fix leak
    void (*onComplete)(const Operation&) = 0;

    Operation& operator&&( Operation& b );
    Operation& operator||( Operation& b );

    State getState() const { return this->state; }
    int getErrorCode() const { return this->errcode; }
    bool isDone() const { return this->state != PENDING; }

    void delay( unsigned long ms );

  };


  class Command : public Operation {

  private:

    friend Air602;
    
    virtual void callback( Air602& mgr, const AT::Payload& resp );
    void step( Air602& mgr );

  public:

    AT::Payload* command;

    Command( String command );
    Command( String command, AT::Operation op, String para );
    ~Command();

    String toString() { return this->command->toString(); }

  };


  class ComplexCommand : public Operation {

  private:

    Operation* left;
    Operation* right;

    const Logic logic;
    
    void step( Air602& mgr );
    
  public:

    ComplexCommand( Logic logic, Operation& a, Operation& b );
    ~ComplexCommand();

  };

  void run( Operation& op );
  void process();
  void flush();

  void setMQTTCallback( void (*callback)(Air602&, const MQTTPayload&) ) {

    this->mqttCallback = callback;

  }

  Air602() {

    // disable echo mode
    //Serial.print("AT+E\r");
    //Serial.flush();
    
  }

  // BOOKMARK: helper functions to create commands
  static Operation& connectAP( String ssid, String key );
  static Operation& setupMQTT( String server, String port,
                               String username, String password );
  
};


#endif
