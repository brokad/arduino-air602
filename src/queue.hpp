#include "Arduino.h"


#define MAX_QUEUE_SIZE 8


template <class T> class Queue {

private:

  T buffer[MAX_QUEUE_SIZE];
  int front, count;

public:

  Queue() { memset(buffer, 0, sizeof(buffer)); }

  bool push(T& e) {

    if( full() ) { return false; }

    buffer[(front + count) % MAX_QUEUE_SIZE] = e;

    count++;

    return true;

  }


  bool pop(int pos=0) {

    if( empty() || (pos >= count) ) { return false; }

    if( pos == 0 ) {

      front = (front + 1) % MAX_QUEUE_SIZE;

    }
    else {

      for( int k = front + pos; k < count - 1; k++ ) {

        buffer[k % MAX_QUEUE_SIZE] = buffer[(k + 1) % MAX_QUEUE_SIZE];
    
      }

    }

    count--;

    return true;

  }


  T& peek( int pos=0 ) {

    int offset = pos/MAX_QUEUE_SIZE + 1;
    pos = (MAX_QUEUE_SIZE * abs(offset) + pos) % MAX_QUEUE_SIZE;

    return buffer[front + pos];

  }


  T& peek_back() {

    return buffer[front + count - 1];

  }


  bool full() {

    return count == MAX_QUEUE_SIZE;

  }


  bool empty() {

    return count == 0;

  }


  int length() {

    return count;

  }

};
