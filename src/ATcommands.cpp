#include <ATcommands.hpp>

using namespace AT;


bool AT::readPayload( Payload& out ) {

  String chunk = read();

  out.type = Type::RESPONSE;

  int control_idx = max(chunk.indexOf('='), chunk.indexOf(':'));

  if( control_idx == -1 ) {

    out.operation = Operation::NONE;

    if( !chunk.endsWith("\r\n\r\n") )
      return false;

    out.header = chunk.substring(0, chunk.indexOf('\r'));
    return true;

  }
  else {

    out.header = chunk.substring(0, control_idx);

    char control = chunk[control_idx];
    chunk.remove(0, control_idx + 1);

    if( control == '=' ) {

      if( !chunk.endsWith("\r\n\r\n") )
        return false;

      char suffix = chunk[1];

      switch(suffix) {

      case '!':
        out.operation = Operation::SET_FLASH;
        chunk.remove(0, 1);
        break;

      case '?':
        out.operation = Operation::GET;
        chunk.remove(0, 1);
        break;

      default:
        out.operation = Operation::SET;
        break;

      }

      out.para = chunk.substring(0, chunk.indexOf('\r'));
      return true;

    }
    else {  // control == ':'

      out.operation = Operation::INFO;

      if( chunk.endsWith("\r\n\r\n") )
        out.para = chunk.substring(0, chunk.indexOf('\r'));

      else
        out.para = chunk;

      return true;

    }

  }

}


String AT::read() {

  String out;
  int time = millis();
  int timeout = 0;
  int termination = 0;

  while( (Serial.available() > 0 || timeout < AT_READTIMEOUT) &&
         Serial.peek() != '+' &&
         !out.endsWith("\r\n\r\n") ) {

    char n = Serial.read();

    if( n != -1 ) {

      out.concat(n);
      timeout = 0;

    }

    int new_t = millis();
    timeout += new_t - time;
    time = new_t;

  }

  return out;

}


AT::Payload::Payload() { }


AT::Payload::Payload( Type type, String header,
                      Operation operation=NONE, String para=String() ):
  type(type),
  header(header),
  operation(operation),
  para(para) { }


String AT::Payload::toString() {

  String o = String();
  String suffix;

  if( this->type == COMMAND ) {
    o.concat("AT+");
    suffix = String("\r");
  }

  else if( this->type == RESPONSE ) {
    o.concat("+");
    suffix = String("\r\n\r\n");
  }

  o.concat(header);

  switch( this->operation ) {
  case Operation::SET: o.concat("="); break;
  case Operation::SET_FLASH: o.concat("=!"); break;
  case Operation::GET: o.concat("=?"); break;
  case Operation::INFO: o.concat(":"); break;
  }

  o.concat(this->para);

  o.concat(suffix);

  return o;

}


int AT::parseParaList( String para, String (&out)[AT_MAX_PARAMETERS] ) {

  int para_count = 0;

  while ( para.length() != 0 ) {

    int comma = para.indexOf(',');

    if ( comma == -1 ) {

      out[para_count] = String(para);
      break;

    }
    else {

      out[para_count] = String(para.substring(0, comma));
      para.remove(0, comma + 1);

    }

    para_count++;

  }

  return para_count;

}
