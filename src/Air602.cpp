#include <Air602.hpp>


void Air602::process() {

  if( Serial.available() > 0 && Serial.read() == '+' ) {

      AT::Payload payload;

      while( AT::readPayload(payload) ) {

        if( payload.operation == AT::Operation::INFO ) {

          if( this->mqttCallback )
            this->handleMQTT(payload);

        }
        else if( !this->queue.empty() ) {

          this->queue.peek().command->callback(*this, payload);
          this->queue.pop();

        }

      }

  }

}

void Air602::handleMQTT( AT::Payload& payload ) {

  if( payload.header == "MTOPIC" ) {

    int c = payload.para.indexOf(',');
    int topiclen = payload.para.substring(0, c).toInt();

    if( topiclen != 0 && c + 1 + topiclen <= payload.para.length() ) {

      if( bufferMQTT )
        delete bufferMQTT;

      bufferMQTT = new MQTTPayload();
      bufferMQTT->type = MQTTPayloadType::MESSAGE;
      bufferMQTT->topic = payload.para.substring(c + 1, c + 1 + topiclen);

    }

  }
  else if( payload.header == "MQD" ) {

    if( bufferMQTT ) {

      int c = payload.para.indexOf(',');
      int len = payload.para.substring(0, c).toInt();

      if( len != 0 && c + 1 + len <= payload.para.length() ) {

        bufferMQTT->data = payload.para.substring(c + 1, c + 1 + len);

        this->mqttCallback(*this, *bufferMQTT);

      }

    }

  }
  else {

    MQTTPayload mqttPayload;

    if( payload.header == "MQTT" )
      mqttPayload.type = MQTTPayloadType::MQTTRESULT;

    else if( payload.header == "MSUB" )
      mqttPayload.type = MQTTPayloadType::MSUBRESULT;

    else if( payload.header == "MPUB" )
      mqttPayload.type = MQTTPayloadType::MPUBRESULT;

    else
      return;

    mqttPayload.data = payload.para;

    this->mqttCallback(*this, mqttPayload);

  }

}

void Air602::flush() {

  if ( !this->queue.empty() ) {

    Transaction& head = this->queue.peek();

    if ( millis() - head.sent > AIR602_DELAY_BEFORE_RETRY ) {

      String buf = head.command->toString();

      if ( buf.length() <= Serial.availableForWrite() ) {

        Serial.print(buf);
        Serial.flush();
        head.sent = millis();

      }

    }

  }

}

bool Air602::enqueue( Command& command, unsigned long delay = 0 ) {

  unsigned long stamp = millis() < AIR602_DELAY_BEFORE_RETRY ? 0 :
    millis() - AIR602_DELAY_BEFORE_RETRY + delay;

  Air602::Transaction t = { &command, stamp };

  return this->queue.push(t);

}

void Air602::run( Air602::Operation& op ) {

  op.step(*this);

}


Air602::Command::Command( String command ) : Command( command,
                                                      AT::Operation::NONE,
                                                      String() ) { }

Air602::Command::Command( String command, AT::Operation op, String para) {

  this->command = new AT::Payload( AT::Type::COMMAND, command, op, para );

}

Air602::Command::~Command() {

  delete this->command;

}

void Air602::Command::callback( Air602& mgr, const AT::Payload& resp ) {

  State state = resp.header == "OK" ? OK : ERR;
  int errcode = 0;

  this->payload = new AT::Payload(resp);

  if( state == ERR )
    errcode = resp.para.toInt();

  this->complete(mgr, state, errcode);

}

void Air602::Command::step( Air602& mgr ) {

  if( !this->isDone() )
    mgr.enqueue(*this, this->delay_ms);

}



Air602::ComplexCommand::ComplexCommand( Air602::ComplexCommand::Logic logic,
                                        Air602::Operation& a,
                                        Air602::Operation& b ) : left(&a),
                                                                 right(&b),
                                                                 logic(logic) {

  a.parent = this;
  b.parent = this;

}

Air602::ComplexCommand::~ComplexCommand() {

  delete left;
  delete right;

}

void Air602::ComplexCommand::step( Air602& mgr ) {

  if( !this->isDone() ) {

    if( !this->left->isDone() )
      this->left->step(mgr);

    else {

      if( (this->logic == AND && this->left->state == OK) ||
          (this->logic == OR && this->left->state == ERR) ) {

        if( !this->right->isDone() )
          this->right->step(mgr);

        else
          this->complete(mgr, this->right->state, this->right->errcode);

      }

      else
        this->complete(mgr, this->left->state, this->left->errcode);

    }

  }

}


void Air602::Operation::delay( unsigned long ms ) {

  this->delay_ms = ms;

}

Air602::Operation& Air602::Operation::operator&&( Air602::Operation& b ) {

  Air602::ComplexCommand* out = new Air602::ComplexCommand( AND, *this, b );
  return *out;

}

Air602::Operation& Air602::Operation::operator||( Air602::Operation& b ) {

  Air602::ComplexCommand* out = new Air602::ComplexCommand( OR, *this, b );
  return *out;

}

void Air602::Operation::complete( Air602& mgr, State state, int errcode ) {

  if( !this->isDone() ) {

    this->state = state;
    this->errcode = errcode;

    if( this->onComplete )
      this->onComplete(*this);

    if( this->parent )
      this->parent->step(mgr);
  }

}



Air602::Operation& Air602::connectAP( String ssid, String key ) {

  auto setssid = new Command("SSID", AT::Operation::SET, ssid);
  auto setencry = new Command("ENCRY", AT::Operation::SET, "5");
  auto setkey = new Command("KEY", AT::Operation::SET, "1,0," + key);
  auto join = new Command("WJOIN");

  return *setssid && *setencry && *setkey && *join;

}


Air602::Operation& Air602::setupMQTT( String server, String port,
                                      String username, String password ) {

  auto mqtt_setserver = new Command("MSERVER",
                                    AT::Operation::SET,
                                    port + "," + server);

  auto mqtt_setuser = new Command("MLOGIN",
                                  AT::Operation::SET,
                                  username + "," + password);

  return *mqtt_setserver && *mqtt_setuser;

}
