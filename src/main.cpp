#include "Arduino.h"

#include <Air602.hpp>

Air602* air602;
Air602::Operation* setupOp;

void setup() {

  air602 = new Air602();
  setupOp = new Air602::Command("AT+");

  Serial.begin(9600);

  while(!Serial)
    delay(100);

  air602->run(*setupOp);

}


void loop() {

  air602->process();
  air602->flush();

}
