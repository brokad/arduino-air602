#ifndef __ATCOMMANDS_HPP__
#define __ATCOMMANDS_HPP__

#include "Arduino.h"  // for String and Serial

#define AT_MAX_PARAMETERS 3
#define AT_MAX_PARA_LENGTH 16
#define AT_READTIMEOUT 10


namespace AT
{

  enum Type
    {
     COMMAND,
     RESPONSE
    };

  enum Operation
    {
     NONE,
     SET,        // =
     SET_FLASH,  // =!
     GET,        // =?
     INFO        // :
    };

  class Payload {
    
  public:

    Type type;
    String header = String();
    Operation operation;
    String para = String();

    Payload();

    Payload( Type type, String header,
             Operation operation, String para );

    String toString();

  };

  bool readPayload( Payload& out );

  String read();

  int parseParaList( String para, String (&out)[AT_MAX_PARAMETERS] );

}


#endif
